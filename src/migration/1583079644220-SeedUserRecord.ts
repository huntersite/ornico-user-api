import {MigrationInterface, QueryRunner} from "typeorm";
import { UserEntity } from '../users/entity/user.entity';

export class SeedUserRecord1583079644220 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const userRepo = queryRunner.manager.getRepository(UserEntity);

        const user = userRepo.create({
            username: 'derrickh',
            password: 'huntwin342',
            email: 'derrickalanhunter@gmail.com',
    });

    await userRepo.save(user);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM TABLE users WHERE username = 'derrickh'`);
    }

}
