import {MigrationInterface, QueryRunner} from "typeorm";

export class InitalCreate1583076975190 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
     // tslint:disable-next-line: max-line-length
     await queryRunner.query(
     `CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, username varchar(50) NOT NULL UNIQUE, password varchar(8000) NOT NULL, email varchar(120) NOT NULL, createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, updatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)`,
     )}

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE users`);
    }

}
