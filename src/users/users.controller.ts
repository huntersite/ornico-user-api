import { Controller, Body, Get, Put, Delete, Param, UseGuards} from '@nestjs/common';
import { UsersService } from './users.service';
import { UserEntity } from '../users/entity/user.entity';

@Controller('users')
export class UsersController {

    constructor(private service: UsersService) { }

    @Get()
    getUsers() {
        return this.service.getUsers();
    }

    @Get(':id')
    get(@Param() params) {
        return this.service.getUser(params.id);
    }

    @Put()
    update(@Body() user: UserEntity) {
        return this.service.updateUser(user);
    }

    @Delete(':id')
    deleteUser(@Param() params) {
        return this.service.deleteUser(params.id);
    }
}